package com.achihoang.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "rooms")
public class Room implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    @NotNull
    private Double price;
    @Column(nullable = false)
    private String amount;
    @Column(nullable = false)
    private String time;
    @Email
    private String email;
    @Column(nullable = false)
    private String status = "empty room";
    @Column(nullable = false)
    private Boolean isCheckIn = false;
    @Column(nullable = false)
    private Boolean isCheckOut = false;
    private String image;
    @Basic
    @Temporal(TemporalType.DATE)
    private Date dateCheckIn ;
    @Basic
    @Temporal(TemporalType.DATE)
    private Date dateCheckOut;

    @ManyToOne
    @JoinColumn(name = "categories_id")
    private Category category;

    public Room(){}

    public Room(String name, Double price, String amount, String time, String email){
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.time = time;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getCheckIn() {
        return isCheckIn;
    }

    public void setCheckIn(Boolean checkIn) {
        isCheckIn = checkIn;
    }

    public Boolean getCheckOut() {
        return isCheckOut;
    }

    public void setCheckOut(Boolean checkOut) {
        isCheckOut = checkOut;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDateCheckIn() {
        return dateCheckIn;
    }

    public void setDateCheckIn(Date dateCheckIn) {
        this.dateCheckIn = dateCheckIn;
    }

    public Date getDateCheckOut() {
        return dateCheckOut;
    }

    public void setDateCheckOut(Date dateCheckOut) {
        this.dateCheckOut = dateCheckOut;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
