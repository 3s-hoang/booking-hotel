package com.achihoang.repository;

import com.achihoang.model.Room;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends PagingAndSortingRepository<Room,Long> {
    @Modifying
    @Query(nativeQuery = true,value = "select * from Rooms r where r.status='empty room' and r.categories_id = ?1")
    Iterable<Room> selectEmptyRoomCategory(Long id);
}
