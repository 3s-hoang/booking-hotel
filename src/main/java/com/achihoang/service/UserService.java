package com.achihoang.service;

import com.achihoang.model.User;
import com.achihoang.model.UserRegistration;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User findByEmail(String email);

    User save(UserRegistration registration);
}
