package com.achihoang.service;

import com.achihoang.model.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface RoomService {
    Page<Room> findAll(Pageable pageable);
    Optional<Room> findById(Long id);
    void save(Room room);
    void delete(Long id);
    Iterable<Room> findAll();

    Iterable<Room> selectEmptyRoomCategory(Long id);
}
