package com.achihoang.controller;

import com.achihoang.model.Category;
import com.achihoang.model.Room;
import com.achihoang.service.CategoryService;
import com.achihoang.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RoomBookController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RoomService roomService;

    @GetMapping(value = "/Rooms/{id}", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView Rooms(@PathVariable("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("/home/index/rooms");
        Iterable<Category> categories = categoryService.findAll();
        modelAndView.addObject("category", categories);
        Iterable<Room> rooms= roomService.selectEmptyRoomCategory(id);
        modelAndView.addObject("listRoomName",rooms);
        return modelAndView;
    }
}
