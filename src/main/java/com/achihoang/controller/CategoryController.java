package com.achihoang.controller;

import com.achihoang.model.Category;
import com.achihoang.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping(value = "/category", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView list(Pageable pageable) {
        ModelAndView modelAndView = new ModelAndView("/admin/category/list");
        Page<Category> categoryPage = categoryService.findAll(pageable);
        modelAndView.addObject("listCategories", categoryPage);
        return modelAndView;
    }

    @GetMapping(value = "/create-category", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/admin/category/create");
        modelAndView.addObject("createCategory", new Category());
        return modelAndView;
    }

    @PostMapping(value = "/create-category", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView saveCreate(@Valid @ModelAttribute("createCategory") Category category, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/category/create");
            return modelAndView;
        } else {
            categoryService.save(category);
            ModelAndView modelAndView = new ModelAndView("/admin/category/create");
            modelAndView.addObject("createCategory", new Category());
            modelAndView.addObject("message", "New Category created successfully");
            return modelAndView;
        }
    }

    @GetMapping(value = "/edit-category/{id}", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView showEditForm(@PathVariable Long id) {
        Optional<Category> category = categoryService.findById(id);
        if (category != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/category/edit");
            modelAndView.addObject("editCategory", category);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("/error/404");
            return modelAndView;
        }
    }

    @PostMapping(value = "/edit-category", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView Edit(@Valid @ModelAttribute("editCategory") Category category, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/category/edit");
            return modelAndView;
        }
        categoryService.save(category);
        ModelAndView modelAndView = new ModelAndView("/admin/category/edit");
        modelAndView.addObject("editCategory", category);
        redirectAttributes.addFlashAttribute("message", "Category updated successfully");
        return new ModelAndView("redirect:category");
    }
}
