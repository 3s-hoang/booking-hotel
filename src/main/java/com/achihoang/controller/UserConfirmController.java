package com.achihoang.controller;

import com.achihoang.model.User;
import com.achihoang.model.UserRegistration;
import com.achihoang.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class UserConfirmController {
    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserRegistration userRegistration() {
        return new UserRegistration();
    }

    @GetMapping(value = "/registration")
    public ModelAndView showRegistrationForm( )
    {
        ModelAndView modelAndView = new ModelAndView("/home/corfirm/signup");
        return modelAndView;
    }
    @PostMapping(value = "/registration")
    public ModelAndView registerUserAccount(@ModelAttribute("user") @Valid UserRegistration userDto, BindingResult result) {
        User existing = userService.findByEmail(userDto.getEmail());
        if (existing != null) {
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        if (result.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("/home/corfirm/signup");
            return modelAndView;
        }

        userService.save(userDto);
        ModelAndView modelAndView = new ModelAndView("/home/corfirm/signup");
        modelAndView.addObject("message","You're successfully registered to our awesome app!");
        return modelAndView;
    }
    @GetMapping("/login")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView("/home/corfirm/login");
        return modelAndView;
    }

    @PostMapping("/login")
    public ModelAndView loginIndex(@ModelAttribute("userlogin")User user) {
        ModelAndView modelAndView = new ModelAndView("/home/corfirm/login");
        userService.findByEmail(user.getEmail());
        return new ModelAndView("redirect:/home");
    }
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login";
    }
}
