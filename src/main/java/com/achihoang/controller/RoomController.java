package com.achihoang.controller;

import com.achihoang.model.Category;
import com.achihoang.model.Room;
import com.achihoang.model.RoomUpload;
import com.achihoang.service.CategoryService;
import com.achihoang.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

@Controller
public class RoomController {

    private static String UPLOAD_LOCATION = "E:\\achihoang\\codegym\\hotelreception\\booking-hotel\\src\\main\\webapp\\WEB-INF\\views\\home\\theme\\images\\";

    @Autowired
    private RoomService roomService;

    @Autowired
    private CategoryService categoryService;

    @ModelAttribute("categories")
    public Iterable<Category> categories(){
        return categoryService.findAll();
    }

    @GetMapping(value = "/room", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView list(Pageable pageable) {
        ModelAndView modelAndView = new ModelAndView("/admin/room/list");
        Page<Room> roomPage = roomService.findAll(pageable);
        modelAndView.addObject("listRooms", roomPage);
        return modelAndView;
    }

    @GetMapping(value = "/create-room", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView showCreateForm() {
        ModelAndView modelAndView = new ModelAndView("/admin/room/create");
        modelAndView.addObject("createRoom", new RoomUpload());
        return modelAndView;
    }

    @PostMapping(value = "/create-room", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView saveCreate(@Valid @ModelAttribute("createRoom") RoomUpload roomUpload, BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/room/create");
            return modelAndView;
        } else {
            MultipartFile file = roomUpload.getMultipartFile();
            String path =UPLOAD_LOCATION + file.getOriginalFilename();
            try {
                FileCopyUtils.copy(file.getBytes(), new File(path));
            } catch (IOException e) {
                e.printStackTrace();
            }
            String pathFile = file.getOriginalFilename();
            Room roomdb = new Room();
            roomdb.setId(roomUpload.getId());
            roomdb.setName(roomUpload.getName());
            roomdb.setPrice(roomUpload.getPrice());
            roomdb.setEmail(roomUpload.getEmail());
            roomdb.setCheckIn(roomUpload.getCheckIn());
            roomdb.setCheckOut(roomUpload.getCheckOut());
            roomdb.setCategory(roomUpload.getCategory());
            roomdb.setImage(pathFile);
            roomdb.setAmount(roomUpload.getAmount());
            roomdb.setTime(roomUpload.getTime());
            roomService.save(roomdb);
            ModelAndView modelAndView = new ModelAndView("/admin/room/create");
            modelAndView.addObject("createRoom", new RoomUpload());
            modelAndView.addObject("message", "New Room created successfully");
            return modelAndView;
        }
    }

    @GetMapping(value = "/edit-room/{id}", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView showEditForm(@PathVariable Long id) {
        Optional<Room> room = roomService.findById(id);
        if (room != null) {
            ModelAndView modelAndView = new ModelAndView("/admin/room/edit");
            modelAndView.addObject("editRoom", room);
            return modelAndView;
        } else {
            ModelAndView modelAndView = new ModelAndView("/error/404");
            return modelAndView;
        }
    }

    @PostMapping(value = "/edit-room", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView Edit(@Valid @ModelAttribute("editRoom") Room room, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasFieldErrors()) {
            ModelAndView modelAndView = new ModelAndView("/admin/room/edit");
            return modelAndView;
        }
        roomService.save(room);
        ModelAndView modelAndView = new ModelAndView("/admin/room/edit");
        modelAndView.addObject("editRoom", room);
        redirectAttributes.addFlashAttribute("message", "room updated successfully");
        return new ModelAndView("redirect:room");
    }
}
