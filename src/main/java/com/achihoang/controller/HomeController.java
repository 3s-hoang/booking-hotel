package com.achihoang.controller;

import com.achihoang.model.Category;
import com.achihoang.model.Room;
import com.achihoang.service.CategoryService;
import com.achihoang.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RoomService roomService;

    @GetMapping(value = "/home", produces = "application/x-www-form-urlencoded;charset=UTF-8")
    public ModelAndView Home() {
        ModelAndView modelAndView = new ModelAndView("/home/index/index");
        Iterable<Category> categories = categoryService.findAll();
        modelAndView.addObject("category", categories);
        Iterable<Room> rooms = roomService.findAll();
        modelAndView.addObject("categoryJoinRoom",rooms);
        return modelAndView;
    }
}
